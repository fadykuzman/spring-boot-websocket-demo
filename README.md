# Spring Boot Websocket Demo

To use a Spring Boot application as a STOMP (over Websocket) server. Follow the steps outlined below.

```text
DISCLAIMER: This is strongly adapted from the Spring Boot Getting Started Guide
```
## Part 1: Simple Websocket implementation

### Step 1: Initialise your application
#### Dependencies
Spring Boot Starter package for Websocket
```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-websocket</artifactId>
 </dependency>
 ```
### Step 2: Configuration

There are different concepts that should be kept in mind to understand the configuration.
For the client to be able to establish a connection, it needs to send the following HTTP request to an endpoint.
```http request
GET /my-websocket-endpoint HTTP/1.1
Host: localhost:8080
Upgrade: websocket
Connection: Upgrade
Sec-WebSocket-Key: Uc9l9TMkWGbHFD2qnFHltg==
Sec-WebSocket-Protocol: v10.stomp, v11.stomp
Sec-WebSocket-Version: 13
Origin: http://localhost:8080
```
The `/my-websocket-endpoint` is the endpoint for establishing the connection.

The STOMP protocol is a frame based protocol. Which means it sends or receives frames (or packets) of information.
How this happens is by using a Message Broker and destinations on that message broker.
Sending a message from client to server, the client send a `SEND` frame that looks like below:

```http request
SEND
destination:/app/hello
content-type:text/plain

hello queue a
^@
```

```text
Check the STOMP protocol documentation for more information.
```

For the client to receive a message form the server, it needs to subscribe to a destination.
Like such:
```http request
SUBSCRIBE
id:0
destination:/topic/greetings
ack:client

^@
```

Then when the server sends a message to the destination `/topic/greetings`, the client receives the following frame:

```http request
MESSAGE
subscription:0
message-id:007
destination:/topic/greetings
content-type:text/plain

hello queue a^@
```

With that knowledge, let's configure the Spring Boot application.
All you need to do, is:
1. generating a configuration class:
2. Annotate it with `@EnableWebSocketMessageBroker`. and implement the `WebSocketMessageBrokerConfigurer`.
3. Override the methods: `registerStompEndpoints(..)` and `configureMessageBroker(..)`.

This is how it should look.
```java
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;

@Configuration
@EnableWebSocketMessageBroker
public class WebsocketConfig implements WebSocketMessageBrokerConfigurer {
    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
        registry.addEndpoint("/my-websocket-endpoint");                      //(1)
    }

    @Override
    public void configureMessageBroker(MessageBrokerRegistry registry) {
        registry.enableSimpleBroker("/topic");                              //(2)
        registry.setApplicationDestinationPrefixes("/app");                 //(3)
    }
}
```
(1) This is the HTTP handshake endpoint or the endpoint to establish the Websocket connection.
(2) This enables the in-memory broker with the arbitrary prefix `/topic`.
    You can also a production ready broker like RabbitMQ, ActiveMQ or Mosquitto.
    `/topic` pattern is just a convention. This is usually used for broadcasting purposes.
    Another convention is `/queue` which is use for one-to-one communication.
(3) This specifies the prefixes for the application destination. i.e. Messages targeted for the application.

### Step 3: Websocket Endpoints
Now we will expose some destinations, which the client can subscribe and send frames/messages to.
First, you have to generate a controller class, that is annotated with `@Controller`.
Then, you write a method that does something and annotated with either or both of the following annotations:
- `@MessageMapping` --> This defines the destination(s) to which the client sends messages.
- `@SendTo` --> This defines the destination(s) to which the client can subscribe and receive messages on.

#### Wiring the destinations
Let's say, your client wants to send messages to the destination `/app/hello`.  
And receive messages on `/topic/greetings`.

Then this should look like the following:
```java
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;
import org.springframework.web.util.HtmlUtils;

@Controller
public class DemoController {
    @MessageMapping("/hello")
    @SendTo("/topic/greetings")
    public Greeting greeting(Hello hello){
        return Greeting.setGreeting(hello);
    }
}
```
#### Message Transfer Objects
We need two "message" objects.
The Hello Pojo which defines the structure of a message received by the server from the client.
```java
public class Hello {
    String name;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
```

The Greeting Pojo which defines the structure of the message (frame body) sent to the client.
```java
public class Greeting {
    String content;

    public Greeting(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public static Greeting setGreeting(Hello hello) {
        return new Greeting("Hello, %s!".formatted(HtmlUtils.htmlEscape(hello.name)));
    }
}
```

#### The Client
Great! You have built your first server. But wait... How do I communicate.
At the time of writing, I adapted the stompJs example from the Spring Boot Getting Started guide.
Hence, for the example to work, copy both the `index.html` and `app.js` files from `resources/static/` directory.

The relevant parts for the communication are:
1. Establishing the connection. HTTP Handshake
```javascript
const stompClient = new StompJs.Client({
    brokerURL: 'ws://localhost:8080/my-websocket-endpoint'
});
```

2. Subscribing to the destination `/topic/greetings`
```javascript
stompClient.onConnect = (frame) => {
    setConnected(true);
    console.log('Connected: ' + frame);
    stompClient.subscribe('/topic/greetings', (greeting) => {
        showGreeting(JSON.parse(greeting.body).content);
    });
};
```
3. Sending frames to the destination `/app/hello`
```javascript
function broadcast() {
    stompClient.publish({
        destination: "/app/hello",
        body: JSON.stringify({'name': $("#name").val()})
    });
}
```
---
### Step 4: Trying the application
#### With one client
1. Open your browser.
2. Open your browser's developer toolbox and go to the __Network__ tab.
3. Navigate to `http://localhost:8080`. You will see a webpage that looks like the following:
![Start Page](./images/start-page.png)
4. Press on the connect button in the top left. You will see the `101` HTTP response, which signifies that the connection is established.
![101 HTTP RESPONSE](./images/101-http-response.png)
5. Click on it and you will find a number of frames. (CONNECT, CONNECTED and SUBSCRIBE). This is what is expected.
```text
CONNECT
accept-version:1.2,1.1,1.0
heart-beat:10000,10000

^@
```
```text
CONNECTED
version:1.2
heart-beat:0,0

^@
```
```text
SUBSCRIBE
id:sub-1
destination:/topic/greetings

^@
```

6. Now write anything in the text field and press on the __Broadcast__ button.
   You should notice 2 things.
    First, what you typed, with a hello message.

    ![Greeting message](./images/greeting-message.png)

    Second, the `MESSAGE` frame in the developer tools.
    ```text
    MESSAGE
    destination:/topic/greetings
    content-type:application/json
    subscription:sub-1
    message-id:522d6b88-3e03-8731-419d-71342ffcee21-0
    content-length:26

    {"content":"Hello, Jane!"}^@
    ```
#### With two clients
1. Open a second browser (That will act as a second client)
2. Now repeat all the steps from above and you should see the same in both browsers
---
## Part2: Sending user specific messages
Broadcasting is definitely beneficial.
However, not every `MESSAGE` is for broadcasting. On occasion, the user wants to receive "personal" messages.

Let's learn!  

### Step 1: Create two users
This is a Spring Security setup. If you are not familiar. Just follow the steps and learn about Spring Security later.

#### Add dependencies
All you need is the security starter artifact. Add it in the `pom.xml` and load the changes.
```xml
    <dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-starter-security</artifactId>
    </dependency>
```

#### Adding users
1. Make a Security configuration class as follows.

```java
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;

@Configuration
@EnableWebSecurity
public class SecurityConfig {
    @Bean
    public UserDetailsService userDetailsService() {
        UserDetails jane =
                User.builder()
                        .username("Jane")
                        .password(encoder().encode("j1234"))
                        .roles("USER")
                        .build();

        UserDetails diana =
                User.builder()
                        .username("Diana")
                        .password(encoder().encode("d1234"))
                        .roles("USER")
                        .build();

        return new InMemoryUserDetailsManager(jane, diana);
    }

    @Bean
    public PasswordEncoder encoder() {
        return new BCryptPasswordEncoder();
    }

}
```

3. Add the following endpoint in the `DemoController`
```java
@Controller
public class DemoController {
    //...
    @MessageMapping("/to.user")                                              //(1)
    @SendToUser("/queue/to.me")                                         //(2)
    public Greeting me(@AuthenticationPrincipal Principal principal) {  //(3)
        return Greeting.setGreeting(new Hello(principal.getName()));    //(4)  
    }
    //...
}
```
(1) Just another destination on which the server would receive frames/messages from the client.
(2) `@SendToUser` is the same as the `@SendTo` in that it is the destination, which the server sends the client frames to.
    The `@SendToUser` does 2 things:
       a. Prepends the string `/user` to `/topic/hi.me`. So it becomes `/user/topic/hi.me`
       b. Appends a user specific id. e.g. `/user/topic/hi.me-user123`. [Read more...](https://docs.spring.io/spring-framework/reference/web/websocket/stomp/user-destination.html)
(3) `@AuthenticationPrincipal` is a Spring Security annotation. It fetches the logged-in or authenticated user.
(4) Get the name of the authenticated user.
(note: I added a constructor to the Hello Object to be able to initialize it with a `String`).

4. In the `WebsocketConfig`:
```java
@Configuration
@EnableWebSocketMessageBroker
public class WebsocketConfig implements WebSocketMessageBrokerConfigurer {
    //...

    @Override
    public void configureMessageBroker(MessageBrokerRegistry registry) {
      //...
        registry.enableSimpleBroker("/topic", "/queue");    //(1)
        registry.setUserDestinationPrefix("/user");         //(2)
    }
}
```
(1) Add `/queue` to the simple broker prefixes. As mentioned above, according to convention `/queue` is used for point-to-point communication.
(2) Add the user destination prefix. This can be anything. However, `/user` is usually used by convention.

5. Update your client. 
    a. In `index.html` add a button.
    ```html
    <button id="toMe" class="btn btn-default" type="submit">Say hi!</button>
    ```
    b. In the `app.js` subscribe to the user-specific destination. 
       In the onConnect, add the following:
    ```javascript
    stompClient.subscribe('/user/queue/to.me', (greeting) => {
        showGreeting(JSON.parse(greeting.body).content);
    });
    ```
   c. Add this function:
    ```javascript
    function toMe() {
     stompClient.publish({
         destination: "/app/to.user",
     });
    }
    ```
   d. Add this to the actions at the bottom of the `app.js`
    ```javascript
        $( "#toMe" ).click(() => toMe());
    ```
6. Open 2 browsers. Login in one browser as Jane and in the other as Diana.
   a. Connect in both sessions -> Write something in the text field -> press "Broadcast" -> Message should appear in both browsers.
   b. Stay in the browsers -> press "Say Hi!" -> Message should only appear in the same browser with the name of the logged in user.

   
It is important to mention that the user-specific destination is automatically mapped by Spring Boot, when `@SendToUser` annotates the method.
The session-id is appended to the destination.
For example, for `@SendToUser("/queue/to.me")` is mapped to `/user/queue/to.me-user<session-id>` where `<session-id>` is the id set in the [`Message`](https://docs.spring.io/spring-framework/docs/6.0.13/javadoc-api/org/springframework/messaging/Message.html) headers.

__To check that yourself:__
1. Activate debugging, by adding the following to `application.yml` or `application.properties`
```yaml
debug: true
```
2. Run the application -> log in -> Press the "Connect" button -> Press the "Say Hi" button.
3. In the logs you will see something like the following. Notice the session id and the `SUBSCRIBE` and `SEND` destination:
```shell

2023-10-17T11:34:31.747+02:00 DEBUG 1357 --- [nboundChannel-2] org.springframework.web.SimpLogging      : Processing CONNECT user=Diana session=1b86fdf2-e9dc-a363-2a2e-c5c296de673c
2023-10-17T11:34:31.755+02:00 DEBUG 1357 --- [nboundChannel-9] org.springframework.web.SimpLogging      : Processing SUBSCRIBE destination=/queue/to.me-user1b86fdf2-e9dc-a363-2a2e-c5c296de673c subscriptionId=sub-1 session=1b86fdf2-e9dc-a363-2a2e-c5c296de673c user=Diana payload=byte[0]
2023-10-17T11:34:37.692+02:00 DEBUG 1357 --- [boundChannel-13] .WebSocketAnnotationMethodMessageHandler : Searching methods to handle SEND /app/to.user session=1b86fdf2-e9dc-a363-2a2e-c5c296de673c, lookupDestination='/to.user'
```

---
## Tidbits:
### Easy Mapping and routing 
Consider the following server setup method:
```java
@Configuration
@EnableWebSocketMessageBroker
public class WebSocketConfig implements WebSocketMessageBrokerConfigurer {

	@Override
	public void registerStompEndpoints(StompEndpointRegistry registry) {
		registry.addEndpoint("/portfolio");
	}

	@Override
	public void configureMessageBroker(MessageBrokerRegistry registry) {
		registry.setApplicationDestinationPrefixes("/app");
		registry.enableSimpleBroker("/topic");
	}
}

```
```java
@Controller
public class DemoController {
    @MessageMapping("/hello1")
    public Greeting postGreeting(Hello hello) {
        return Greeting.setGreeting(hello);
    }
}
```
When a client sends a `SEND` frame on the destination `/app/hello1` to the server. 
Spring strips the `/app` prefix and it is mapped to `@MessageMapping`.
The server then translates the frame body to a `Message` object and passes it through the `clientInboundChannel` with a default destination header `/topic/hello1`.
When the `@SendTo` is missing, then Spring, by default, prepends the destination with `/topic`. (This depends on your configuration of your server.)



---

If you want me to cover the following, write me in the comments.
---
# Variations
- [ ] Customizable user-specific destinations
- [ ] Send an HTTP Request and broadcast a message
- [ ] Send an HTTP Request and only a specific user receives an event/message
- [ ] Secure an endpoint for certain roles