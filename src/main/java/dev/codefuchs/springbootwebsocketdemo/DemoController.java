package dev.codefuchs.springbootwebsocketdemo;

import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.annotation.SendToUser;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;

import java.security.Principal;

@Controller
public class DemoController {
    @MessageMapping("/hello")
    @SendTo("/topic/greetings")
    public Greeting greeting(Hello hello){
        return Greeting.setGreeting(hello);
    }

    @MessageMapping("/to.user")
    @SendToUser("/queue/to.me")
    public Greeting me(@AuthenticationPrincipal Principal principal){
        return Greeting.setGreeting(new Hello(principal.getName()));
    }
}
