package dev.codefuchs.springbootwebsocketdemo;

import org.springframework.web.util.HtmlUtils;

public class Greeting {
    String content;

    public Greeting(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public static Greeting setGreeting(Hello hello) {
        return new Greeting("Hello, %s!".formatted(HtmlUtils.htmlEscape(hello.name)));
    }
}
