package dev.codefuchs.springbootwebsocketdemo;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;

@Configuration
@EnableWebSecurity
public class SecurityConfig {
    @Bean
    public UserDetailsService userDetailsService() {
        UserDetails jane =
                User.builder()
                        .username("Jane")
                        .password(encoder().encode("j1234"))
                        .roles("USER")
                        .build();

        UserDetails diana =
                User.builder()
                        .username("Diana")
                        .password(encoder().encode("d1234"))
                        .roles("USER")
                        .build();

        return new InMemoryUserDetailsManager(jane, diana);
    }

    @Bean
    public PasswordEncoder encoder() {
        return new BCryptPasswordEncoder();
    }

}
